import java.util.Scanner;

class Program{
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();
		int sum = a % 10 + ((a / 10) % 10) + ((a / 100) % 10) + ((a / 1000) % 10) + ((a / 10000) % 10);
		System.out.println(sum);
	}
}