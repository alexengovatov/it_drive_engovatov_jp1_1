package com.company;

import com.sun.org.apache.bcel.internal.generic.ARETURN;

import java.time.LocalTime;


public class Channel {

    private final int MAX_COUNT_PROGRAMS = 20;
    private Program[] programs;
    private char[] name;
    private int channelNumber;
    private TV tv;
    private int count;
    private LocalTime currentTime;


    Channel (int channelNumber, char [] name){
        this.channelNumber = channelNumber;
        this.programs = new Program[MAX_COUNT_PROGRAMS];
        this.name = name;
        this.count = 0;
        this.currentTime = LocalTime.of(20, 0);

    }

    void addProgram(Program program){
        if (count <= MAX_COUNT_PROGRAMS){
            this.programs[count] = program;
            program.setChannel(this);
            this.count++;
        }
    }

    void setTv(TV tv){
        this.tv = tv;
    }

    Program getProgram(LocalTime currentTime){
        Program program = programs[0];
        for (int i=0; i<programs.length; i++)
            if ((programs[i].getTime().getHour() < currentTime.getHour()) || (programs[i].getTime().getMinute() <= currentTime.getMinute() && programs[i].getTime().getHour() == currentTime.getHour()))
            {
                program = programs[i];
                break;
            }
            return program;
    }

    char [] getName(){
        return name;
    }
    void showProgram(int channelNumber){
        System.out.println(channelNumber+" "+getName().toString());
        System.out.println(getProgram(this.currentTime).getTime()+" "+getProgram(this.currentTime).getName().toString());
    }



}
