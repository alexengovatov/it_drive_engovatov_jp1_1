import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char[] characters = scanner.nextLine().toCharArray();
        for (int i = 0; i < threePopular(characters).length; i++)
            System.out.println(threePopular(characters)[i]);// write your code here
    }

    public static char[] threePopular(char[] a) {
        int counts[] = new int[26];
        char[] result = new char[3];
        int firstLocalMaxCount = 0;
        int secondLocalMaxCount = 0;
        int thirdLocalMaxCount = 0;
        for (int i = 0; i < a.length; i++) {
            if (a[i] >= 'a' && a[i] <= 'z')
                counts[a[i] - 'a']++;
        }
        for (int i = 0; i < counts.length; i++) {
            if (counts[i] > firstLocalMaxCount) {
                thirdLocalMaxCount = secondLocalMaxCount;
                secondLocalMaxCount = firstLocalMaxCount;
                firstLocalMaxCount = counts[i];
                result[0] = (char) (i + 'a');
            } else if (counts[i] > secondLocalMaxCount) {
                thirdLocalMaxCount = secondLocalMaxCount;
                secondLocalMaxCount = counts[i];
                result[1] = (char) (i + 'a');
            } else if (counts[i] > thirdLocalMaxCount) {
                thirdLocalMaxCount = counts[i];
                result[2] = (char) (i + 'a');
            }

        }
        return result;
    }
}


