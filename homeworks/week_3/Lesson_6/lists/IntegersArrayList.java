package com.company;

public class IntegersArrayList {
    private static final int DEFAULT_LIST_SIZE = 10;

    private int elements[];
    private int count;

    public IntegersArrayList() {
        this.elements = new int[DEFAULT_LIST_SIZE];
        this.count = 0;

    }

    public void add(int element) {
        if (this.count == this.elements.length) {
            int temp = (int) (DEFAULT_LIST_SIZE * 1.5);
            int[] tempElements = new int[temp];
            for (int i = 0; i < this.elements.length; i++) {
                tempElements[i] = this.elements[i];
            }
            this.elements = new int[temp];
            for (int i = 0; i < tempElements.length; i++) {
                this.elements[i] = tempElements[i];
            }
        }
            if (count < this.elements.length) {
                elements[count] = element;
                count++;
            } else {
                System.err.println("Array out of Bounds");
            }
        }

        public void add ( int element, int index){
            if (this.count == this.elements.length) {
                int temp = (int) (DEFAULT_LIST_SIZE * 1.5);
                int[] tempElements = new int[temp];
                for (int i = 0; i < this.elements.length; i++) {
                    tempElements[i] = this.elements[i];
                }
                this.elements = new int[temp];
                for (int i = 0; i < tempElements.length; i++) {
                    this.elements[i] = tempElements[i];
                }
            }
            if (count < this.elements.length) {
                for (int i = count; i > index; i--) {
                    this.elements[i] = this.elements[i - 1];
                }
                this.elements[index] = element;
            } else {
                System.err.println("Array out of Bounds");
            }
        }

        public void remove ( int index){
            for (int i = index; i < count; i++) {
                this.elements[i] = this.elements[i + 1];
            }
            this.elements[count] = 0;
        }


        public void reverse () {
            for (int i = 0; i < count / 2; i++) {
                int temp = this.elements[i];
                this.elements[i] = this.elements[count - i - 1];
                this.elements[count - i - 1] = temp;
            }
        }
    }
