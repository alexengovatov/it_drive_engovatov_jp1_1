package com.company;

public class TV {

    private final int MAX_COUNT_OF_CHANNELS = 20;
    private char[] model;
    private Channel [] channels;
    private int count;
    private RemoteController remoteController;

    private int channelNumber;

    TV(char[] model){

        this.model = model;
        this.channels = new Channel[MAX_COUNT_OF_CHANNELS];
        this.count = 0;

    }

    void addChannel(Channel channel){
        if (count <= MAX_COUNT_OF_CHANNELS)
        {
            this.channels[count] = channel;
            channel.setTv(this);
            this.count++;
        }
    }

    void setChannelNumber(int channelNumber){
        this.channelNumber = channelNumber;
    }

    void setRemoteController(RemoteController remoteController){
        this.remoteController = remoteController;
    }

    Channel getChannel(int channelNumber){
        return channels[channelNumber];
    }


}
