public class Program {

    public static void main(String[] args) {
        char[] a = {'a', 'b'};
        char[] b = {'a', 'b', 'c'};
        System.out.println(stringCompare(a, b));// write your code here
    }

    public static int stringCompare(char[] a, char[] b) {
        int length;
        if (a.length < b.length)
            length = a.length;
        else length = b.length;
        int result = 0;
        for (int i = 0; i < length; i++) {
            if (a[i] > b[i]) {
                result = -1;
                break;
            }
            if (a[i] < b[i]) {
                result = 1;
                break;
            }
        }
        if (result == 0 && a.length > b.length)
            result = -1;
        else if (result == 0 && b.length > a.length)
            result = 1;
        return result;
    }