import java.util.Scanner;

class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long counter = 1;
        int number = scanner.nextInt();
        while (number != -1) {
            if (isNumberEven(number))
              counter *= number;
            number = scanner.nextInt();
        }
        System.out.println(counter);

    }
    public static boolean isNumberEven(int number) {
        int sumOfNumbers = 0;
        while (number>=1) {
            sumOfNumbers += number % 10;
            number /= 10;
        }
        if (sumOfNumbers % 2 == 0)
            return true;
        else return false;
    }

}