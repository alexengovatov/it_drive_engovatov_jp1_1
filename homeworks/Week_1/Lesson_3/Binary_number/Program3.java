import java.util.Scanner;

class Program3{
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();
		long c = 0, d = 1;

		while (a >= 1)
		{
			c += (a % 2) * d;
			d *= 10;
			a /= 2; 
		}
		System.out.println(c);
	}
}