import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int ageOfMaxCount = 0;
        int result = 0;
        Human[] humans = new Human[100];
        for (int i = 0; i < humans.length; i++) {
            humans[i] = initializing(new Human());
        }
        int counts[] = new int[80];
        for (int i = 0; i < humans.length; i++) {
            if (humans[i].age == i)
                counts[i - 1]++;
        }
        for (int i = 0; i < counts.length; i++) {
            if (counts[i] > ageOfMaxCount) {
                ageOfMaxCount = counts[i];
                result = i;
            }
        }
        System.out.println(result);

    }

    public static Human initializing(Human human) {
        int size = generateRandomIntInRange(1, 10);
        char[] name = new char[size];
        int age = generateRandomIntInRange(1, 80);
        for (int i = 0; i < name.length; i++) {
            name[i] = generateRandomCharInRange();
        }
        human.name = name;
        human.age = age;
        return human;
    }

    public static int generateRandomIntInRange(int min, int max) {
        Random random = new Random();
        return random.nextInt(((max - min) + 1) + min);
    }

    public static char generateRandomCharInRange() {
        Random random = new Random();
        return (char) (random.nextInt(26) + 'a');
    }

}

class Human {
    int age;
    char[] name;
}

