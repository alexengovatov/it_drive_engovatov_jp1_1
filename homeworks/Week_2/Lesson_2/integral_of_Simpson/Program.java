import java.util.Scanner;

class Program {
	public static void main(String[] args) {
	
	Scanner scanner = new Scanner (System.in);
	double a = scanner.nextDouble();
	double b = scanner.nextDouble();
	int n = scanner.nextInt();	
	
	System.out.format("%12s%10d%c%10f%n", "Simpson", n, ' ', Simpson(a, b, n));
	System.out.format("%12s%10d%c%10f", "rectaingle", n, ' ', rectaingle(a, b, n));

	}

	public static double f(double x) {
		return x * x;
	}
	
	public static double Simpson(double a, double b, int n) {
		
		double result = 0;
		double h = (b - a) / n;	

		for (int i = 1; i <= n - 1; i = i + 2) {
		
			result = result + f(a + (i - 1) * h) + 4 * f(a + i * h) + f(a + (i + 1) * h);
		}
		
		result = result * h / 3;
		return result;
	}

	public static double rectaingle (double a, double b, int n) {

		double result = 0;
		double h = (b - a) / n;

		for (double x = a; x <= b; x = x + h) {
			double currentRectaingle = f(x) * h;
			result += currentRectaingle;
		}

		return result;
	}



}