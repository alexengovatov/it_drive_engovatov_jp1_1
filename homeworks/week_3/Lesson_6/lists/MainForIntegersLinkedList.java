package com.company;

public class MainForIntegersLinkedList {
    public static void main(String[] args) {
        IntegerLinkedList linkedList = new IntegerLinkedList();
        for (int i = 0; i < 10; i++){
            linkedList.add(i);
        }

        System.out.println(linkedList.get(4));

        linkedList.add(5, -100);
        for (int i=0; i<11; i++)
            System.out.println(linkedList.get(i));

        linkedList.reverse();
        for (int i=0; i<11; i++)
            System.out.println(linkedList.get(i));
        
    }
}
