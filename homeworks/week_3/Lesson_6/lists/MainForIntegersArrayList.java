package com.company;

public class MainForIntegersArrayList {
    public static void main(String[] args) {
        IntegersArrayList list = new IntegersArrayList();

        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);
        list.add(11);
        list.add(12);
        list.add(13);
        list.add(14);
        list.add(15);
        list.add(16);
        list.add(17);
        list.add(18);

        list.add(-1, 3);
        list.remove(3);
        list.reverse();

    }
}
