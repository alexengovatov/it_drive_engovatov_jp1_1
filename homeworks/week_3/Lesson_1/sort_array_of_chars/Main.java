import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char[][] list = new char[10][];
        for (int i = 0; i < list.length; i++) {
            list[i] = scanner.nextLine().toCharArray();
        }
        for (int i = 0; i < list.length; i++) {
            for (int j = i; j < list.length; j++)
                if (compare(list[i], list[j]) == -1) {
                    char[] temp = list[j];
                    list[j] = list[i];
                    list[i] = temp;
                }
        }
        for (int i = 0; i < list.length; i++)
            System.out.println(list[i]);
    }

    public static int compare(char[] a, char[] b) {
        int length;
        if (a.length < b.length)
            length = a.length;
        else length = b.length;
        int result = 0;
        for (int i = 0; i < length; i++) {
            if (a[i] > b[i]) {
                result = -1;
                break;
            }
            if (a[i] < b[i]) {
                result = 1;
                break;
            }
        }
        if (result == 0 && a.length > b.length)
            result = -1;
        else if (result == 0 && b.length > a.length)
            result = 1;
        return result;
    }
}