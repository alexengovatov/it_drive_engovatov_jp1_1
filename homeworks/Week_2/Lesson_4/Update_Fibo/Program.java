public class Program {

    public static void main(String[] args) {
        System.out.println(fib(0, 1, 8));// write your code here
    }

    public static int fib(int x1, int x2, int n) {
        if (n > 1) {
            int x3 = x1 + x2;
            x1 = x2;
            x2 = x3;
            return fib(x1, x2, n - 1);
        }
        else return x2;
    }
}