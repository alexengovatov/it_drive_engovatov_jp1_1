public class Main {

    public static void main(String[] args) {
        char[] a = {'2', '3', '1', '1'};
        System.out.println(developIntoInt(a));// write your code here
    }

    public static long developIntoInt(char[] a) {

        int k = 1;
        for (int i = 1; i <= a.length - 1; i++)
            k *= 10;
        long result = 0;
        int i = 0;
        while (k >= 1) {
            result += charIntoDigit(a[i]) * k;
            k /= 10;
            i++;
        }
        return result;
    }

    public static int charIntoDigit(char c) {
        return c - '0';
    }
    
}