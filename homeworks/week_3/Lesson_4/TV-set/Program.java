package com.company;

import java.time.LocalTime;

public class Program {

    private char[] name;
    private LocalTime time;
    private Channel channel;

    void setChannel (Channel channel){
        this.channel = channel;
    }

    Program(char[] name, LocalTime time){
        this.name = name;
        this.time = time;
    }

    char [] getName (){
        return name;
    }

    LocalTime getTime(){
        return time;
    }


}
