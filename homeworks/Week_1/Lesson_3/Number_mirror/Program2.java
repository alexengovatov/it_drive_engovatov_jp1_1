import java.util.Scanner;

class Program2{
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();
		int answer = (a % 10) * 10000 + ((a / 10) % 10) * 1000 + ((a / 100) % 10) * 100 + ((a / 1000) % 10) * 10 + ((a / 10000) % 10);
		System.out.println(answer);
	}
}