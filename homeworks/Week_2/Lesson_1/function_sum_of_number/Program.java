import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int k = scanner.nextInt();
		System.out.println(sumOfNumber(k));
	}

	public static int sumOfNumber(int a) {
		int sum = 0;
		while (a >= 1) {
			sum += a%10;
			a /= 10;
		}
		return sum;
	}
}