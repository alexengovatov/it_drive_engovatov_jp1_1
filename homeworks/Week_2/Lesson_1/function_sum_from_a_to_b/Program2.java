import java.util.Scanner;

class Program2 {
	public static void main(String[] args) {
	Scanner scanner = new Scanner(System.in);
	int a = scanner.nextInt();
	int b = scanner.nextInt();
	System.out.println(summary(a, b));
	}


	public static int summary(int a, int b) {
		int sum = 0;
		while (a <= b) {
			sum += a;
			a++;
		}
		return sum;
	}
}