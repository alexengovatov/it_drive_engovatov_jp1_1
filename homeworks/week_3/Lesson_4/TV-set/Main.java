package com.company;

import java.time.LocalTime;

public class Main {

    public static void main(String[] args) {
    Program firstChannelProgram = new Program("Время".toCharArray(), LocalTime.of(21, 0));
    Program firstChannelProgram1 = new Program("На самом деле".toCharArray(), LocalTime.of(18, 30));
    Program firstChannelProgram2 = new Program("Пусть говорят".toCharArray(), LocalTime.of(19, 40));

    Program secondChannelProgram = new Program("Андрей Малахов. Прямой эфир".toCharArray(), LocalTime.of(18, 30));
    Program secondChannelProgram1 = new Program("Вести".toCharArray(), LocalTime.of(20,0));
    Program secondChannelProgram2 = new Program("Вести. Местное время. Кубань".toCharArray(), LocalTime.of(20, 45));

    Program thirdChannelProgram = new Program("КХЛ. ЦСКА - СКА".toCharArray(), LocalTime.of(18, 50));
    Program thirdChannelProgram1 = new Program("Новости".toCharArray(), LocalTime.of(22, 15));
    Program thirdChannelProgram2 = new Program("Волейбол. Лига чемпионов".toCharArray(), LocalTime.of(22, 25));

    Channel firstChannel = new Channel(1, "Первый канал".toCharArray());
    firstChannel.addProgram(firstChannelProgram);
    firstChannel.addProgram(firstChannelProgram1);
    firstChannel.addProgram(firstChannelProgram2);

    Channel russia = new Channel(2, "Россия".toCharArray());
    russia.addProgram(secondChannelProgram);
    russia.addProgram(secondChannelProgram1);
    russia.addProgram(secondChannelProgram2);

    Channel matchtv = new Channel(3, "Матч ТВ".toCharArray());
    matchtv.addProgram(thirdChannelProgram);
    matchtv.addProgram(thirdChannelProgram1);
    matchtv.addProgram(thirdChannelProgram2);

    TV tv = new TV("LG".toCharArray());
    firstChannel.setTv(tv);
    matchtv.setTv(tv);
    russia.setTv(tv);
    tv.addChannel(firstChannel);
    tv.addChannel(russia);
    tv.addChannel(matchtv);

    RemoteController remoteController = new RemoteController("LG".toCharArray());
    tv.setRemoteController(remoteController);
    remoteController.setTv(tv);

    remoteController.on(0);
    }
}
