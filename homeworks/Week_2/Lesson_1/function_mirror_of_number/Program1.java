import java.util.Scanner;

class Program1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int k = scanner.nextInt();
		System.out.println(mirror(k));
	}

	public static int mirror (int a) {
	int result = 0;	
	while (a > 0) {
		result = result*10 + a % 10;
		a /= 10;
	}	
	return result;
	}
}