package com.company;

public class IntegerLinkedList {
    private Node first;
    private Node last;
    private int count;


    public IntegerLinkedList() {
        this.count = 0;
    }

    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            this.first = newNode;
            this.last = newNode;
            first.setPrevious(null);
            last.setNext(null);
        } else {
            //    Node current = first;
            //  while (current.getNext() != null){
            //    current = current.getNext();
            //}
            //current.setNext(newNode);
            newNode.setPrevious(last);
            this.last.setNext(newNode);
            this.last = newNode;
        }
        this.count++;
    }

    public int get(int index) {
        if (index >= 0 && index < this.count) {
            Node current = this.first;
            int countForGet = 0;
            while (countForGet < index) {
                current = current.getNext();
                countForGet++;
            }
            return current.getValue();
        } else return -1;
    }

    public void add(int index, int element) {
        if (index > 0 && index < this.count) {

            Node changed = this.node(index);
            Node prev = changed.getPrevious();
            Node current = new Node(element, changed, prev);
            int countForAdd = 0;
            while (countForAdd < this.count) {
                countForAdd++;
                if (countForAdd == index) {
                    this.count++;
                    changed.setPrevious(current);
                    prev.setNext(current);
                    break;
                }
            }

        } else System.err.println("Index more than count");

    }

    public Node node(int index) {
        if (this.count - index < index) {
            Node current = this.last;
            for (int i = this.count; i > index; i--) {
                current = current.getPrevious();
            }
            return current;
        } else {
            Node current = this.first;
            for (int i = 0; i < index; i++)
                current = current.getNext();
            return current;
        }
    }

    public void reverse() {
        Node a = this.first;
        Node b = this.last;
        int k = this.count / 2;
        int countForReverse = 0;
        while (countForReverse <= k) {
            int tempValue = a.getValue();
            a.setValue(b.getValue());
            b.setValue(tempValue);
            a = a.getNext();
            b = b.getPrevious();
            countForReverse++;
        }
    }

    public void remove(Node node) {
        int number = 0;
        Node current = this.node(0);
        for (int i = 0; i < this.count - 1; i++) {
            if (this.get(i + 1) == node.getValue()) {
                this.node(i + 1).setValue(this.get(i + 2));
                number = i + 1;
            }
        }
        for (int i = number; i < this.count - 1; i++) {
            this.node(i).setNext(this.node(i+1));
        }


    }


}

