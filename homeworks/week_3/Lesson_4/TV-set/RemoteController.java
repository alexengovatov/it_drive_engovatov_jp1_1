package com.company;

public class RemoteController {

    private int channelNumber;
    private TV tv;
    private char[] name;

    RemoteController(char [] name){
        this.name = name;
    }

    void setTv(TV tv){
        this.tv = tv;
    }

    void on(int channelNumber){
        this.channelNumber = channelNumber;
        tv.setChannelNumber(channelNumber);
        tv.getChannel(channelNumber).showProgram(channelNumber);
        }
}
